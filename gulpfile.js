const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const filter = require('gulp-filter');
const copy = require('gulp-copy');
const zip = require('gulp-zip');
const path = require('path');

const root = path.dirname();
const assets = path.resolve(root, 'assets');
const public = path.resolve(root, 'public');

gulp.task('sass', function() {
  return gulp.src(path.resolve(assets, 'scss', '**/*.scss'))
    .pipe(sass())
    .pipe(gulp.dest(path.resolve(assets, 'css')))
});

gulp.task('watch', ['sass'], function() {
  return gulp.watch(path.resolve(assets, 'scss', '**/*.scss'), ['sass']);
});

gulp.task('copy-images', function(){
  return gulp.src(path.resolve(assets, 'images', '**'))
    .pipe(gulp.dest(path.resolve(public, 'images')))
});

gulp.task('copy-fonts', function(){
  return gulp.src(path.resolve(assets, 'fonts', '**'))
    .pipe(gulp.dest(path.resolve(public, 'fonts')));
});

// build to public folder
gulp.task('build', ['copy-images', 'copy-fonts'], function(){
  gulp.src([
    path.resolve(assets, 'css', 'normalize.min.css'),
    path.resolve(assets, 'css', 'font-awesome.min.css'),
    path.resolve(assets, 'css', 'main.css')
  ])
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest(public));

  gulp.src(path.resolve(assets, 'js', '**/*.js'))
    .pipe(uglify())
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest(public));
});


// pack the app to an zip
gulp.task('pack', function(){
  // api
  // config
  // public
  // views
  // .sailsrc
  // app.js
  // package.json
  // README.md

  gulp.src([
    'api/**',
    'config/**',
    'public/**',
    'views/**',
    '.sailsrc',
    'app.js',
    'package.json',
    'README.md'
  ], {base: '.'})
    .pipe(zip('pack.zip'))
    .pipe(gulp.dest(root))
});
