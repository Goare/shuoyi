module.exports = function(req, res, next)
{
  var lang = req.query.lang;

  if (lang)
  {
    req.setLocale(lang)
  }
  return next();
};
